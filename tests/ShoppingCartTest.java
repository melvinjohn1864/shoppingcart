import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ShoppingCartTest {
	
	ShoppingCart cart;
	Product phone;
	Product hamburger;
	
	
	@Before
	public void setUp() throws Exception {
		
		// 1. Make a new cart
			cart = new ShoppingCart();
			
		//Create products to use in all the test cases.
			phone = new Product("iphone", 1500);
			hamburger = new Product("burger", 10);
		
	}

	@Test
	public void testCreateCart() {
		// Test: When created, the cart has 0 items 

		
		// 1. check number of itmes in cart
		int a = cart.getItemCount();
		
		// 2. do the assert
		assertEquals(0, a);
	}
	
	// -----------------------------
	// R2: Empty carts have 0 items
	// -----------------------------

	@Test
	public void testEmptyTheCart() {
		// 1. Add an item to the cart
		cart.addItem(phone);
		cart.addItem(hamburger);
		
		// 2.Get rid of all items in the cart
		cart.empty();
		
		// 3. Check num items in cart === E = 0
		assertEquals(0, cart.getItemCount());
	}
	
	// -------------------------------
	// R3: Adding items to cart
	// -------------------------------
	@Test
	public void testAddProductToCart() {
		// EO1: When a new product is added, 
		// 		the number of items must be incremented  
		// EO2: When a new product is added, 
		// 		the new balance must be the 
		// 		sum of the previous balance plus 
		// 		the cost of the new product
		// ----------------------------
		
		
		//    1.  CHECK BALANCE IN CART BEFORE ADDING PRODUCT
		//    			- PREV BAL
		double startBalance = cart.getBalance();
		assertEquals(0, startBalance, 0.01);

		//    2.  CHECK NUM ITEMS IN CART BEFORE ADDING PRODUCT
		//   		    - PREV NUM ITEMS
		int startingNumItems = cart.getItemCount();
		assertEquals(0, startingNumItems);
		
		//    3.  ADD THE PRODUCT TO THE CART 
		cart.addItem(phone);
		
		 //    4.  CHECK THE UPDATED NUMBER OF ITEMS IN CART 
		 //    		-- EO: PREV NUM ITEMS + 1
		// OPTION 1: A resuable, maintabale, great solution
		assertEquals(startingNumItems + 1, cart.getItemCount());
		// OPTION 2: Okay, but not very resuable (ok but not ok)
		// assertEquals(1, cart.getItemCount());
		
		 //    -----------------------
		 //    5.  CHECK THE UPDATED BALANCE OF THE CART
		 //   		-- EO: PREVIOUS BALANCE + PRICE OF PRODUCT
		double expectedBalance = startBalance + phone.getPrice();
		assertEquals(expectedBalance, cart.getBalance(), 0.01);
		
	}

}
